<!--

# The issue title should be: Product Designer Onboarding (NAME), as (ROLE, STAGE GROUP)

-->

/assign (FILL IN WITH @ NEW TEAM MEMBER HANDLE) (FILL IN WITH @ UX BUDDY HANDLE)
/due (FILL IN WITH 1 MONTH AFTER START DATE)
/label ~onboarding 
/confidential

# Hello :wave: 

Welcome to the UX department (FILL IN WITH @ NEW TEAM MEMBER HANDLE)! We are happy to have you and support you! :slight\_smile:

To get you up to speed so you can start contributing to the team efforts and improving GitLab as a product, this onboarding issue will try to provide a few tips on how to navigate around the team resources as well as company resources.

First, you are likely to want to know where your place is in the company structure. For this, you would want to check out the [team chart](https://about.gitlab.com/team/chart/).

If you have questions, don't hesitate to mention (FILL IN WITH @ UX BUDDY HANDLE) (your [UX buddy](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/#ux-design-buddy)), (FILL IN WITH @ PRODUCT DESIGN MANAGER HANDLE) (your Product Design Manager), (FILL IN WITH @ TEAM GITLAB HANDLE) (your UX team) or the whole UX department by using `@gitlab\-com/gitlab\-ux` (use the latter sparingly unless it's something important). You can also reach out to people in the [#ux Slack channel](https://gitlab.slack.com/messages/C03MSG8B7/).

You'll have a weekly 1:1 call with your UX buddy until your onboarding is complete! Fridays are great for these to talk through questions and set expectations for the next week. Your onboarding doesn't have a scheduled end date, just focus on learning and getting comfortable with GitLab and the UX department.

## Company :earth\_africa:

Most of the information you will ever need is listed in your general onboarding document. However, there are a lot of things to digest in there so let's highlight a few items that you should focus on.

- [ ] The [handbook](https://about.gitlab.com/handbook/) is a great resource for all things GitLab. Understanding the [Values](https://about.gitlab.com/handbook/values/) is really important as that sets the tone for all interactions that you will have in the company. If anything is unclear about it, feel free to ask a question in this issue or in your next 1:1 with your UX buddy or Product Design Manager.
- [ ] [Learn how we communicate as a company](https://about.gitlab.com/handbook/communication/). We use [asynchronous communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/) as a start and are as open as we can be by communicating through public issues, chat channels, and placing an emphasis on ensuring that conclusions of offline conversations are written down. 
- [ ] Because of GitLab's unique asynchronous culture and communication style, it's likely that many of your doubts regarding processes and internal workflows are already documented somewhere. Be sure to check the ["Searching like a Pro"](https://about.gitlab.com/handbook/tools-and-tips/searching/) guide, and the ["Search Through GitLab"](https://docs.gitlab.com/ee/user/search/) entry in the GitLab Docs.
- [ ] Sooner or later you will have some money related questions, so be sure to check out the ["Spending Company Money"](https://about.gitlab.com/handbook/spending-company-money/) page.

Of course, it would be great to go through the whole handbook but that is going to be really difficult given the size of it so don't feel bad if you can't read through all of it. Remember that you can always search! For now, though, you should get to know your team a bit better.

## UX Department :raised\_hands:

Each department has its own handbook section, and UX is no exception:
- [ ] Read through the whole [UX handbook](https://about.gitlab.com/handbook/product/ux/) section, please!
   - Consider this your first team task, understanding the Mission, Vision and how work is executed within the department. If you find any typos or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/sites/handbook/source/handbook/product/ux/index.html.md.erb) by submitting a merge request. 
- [ ] Join the `#random-coffees-ux-dept` Slack channel to automatically be paired with someone from the UX department for a [coffee chat](https://about.gitlab.com/company/culture/all-remote/#coffee-chats). Pairings occur once every two weeks. Coffee chats help get to know others you work with and talk about everyday things. We want you to make friends and build relationships with the people you work with to create a more comfortable, well-rounded environment. 
In and around your third week, your UX buddy asks you to shadow them for their daily UX activities. This is a good primer and a way to get more familiar with product designer workflows. You will serve as a second opinion and be included in meetings.

To highlight some other important places:
- [ ] Read the [UX department workflow](https://about.gitlab.com/handbook/product/ux/ux-department-workflow/) which is important to get to know which dates matter and to align yourself to our development rhythm.
- [ ] Read the [design project and documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md). Verify that you have access to Figma. Request access to [Mural](https://app.mural.co/) from your Product Design Manager and ask about any other tools you might need.
- [ ] Ask your Product Design Manager to make you an editor for the Figma team of your stage.
- [ ] Become familiar with [Pajamas](http://design.gitlab.com), which is our design system where we document all product design decisions and things to help us make those decisions, like [personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/).

You can [open an issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new) in the repository to start a discussion on design-related matters. For new component design or existing component updates, [open an issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/new) in the Pajamas repository. For product-related matters, the [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/new) is the correct place.

The UX department is supported by Product Operations:
- [ ] Review [product operations in the product handbook](https://about.gitlab.com/handbook/product/product-operations/) and set up a coffee chat with with Product Operations `@fseifoddini`
- [ ] Ask questions in the #product-operations or #product Slack channels to get clarification on things you are uncertain about
- [ ] *Optional* Attend [PM Meeting bi-weekly and Product Operations bi-weekly meetings](https://about.gitlab.com/handbook/product/product-processes/#team-meetings) (they alternate and are on the company calendar)


## Staying informed :mega:

- [ ] Consider joining these Slack channels to stay on top of the most important things
    -  UX Department
        - [#ux](https://gitlab.slack.com/messages/C03MSG8B7)
        - [#ux-coworking](https://gitlab.slack.com/messages/CLW71KM96)
        - [#ux-research](https://gitlab.slack.com/messages/CMEERUCE4)
        - [#design-system](https://gitlab.slack.com/messages/CDNNDD1T3)
        - [#ux-bookclub](https://gitlab.slack.com/messages/CRCQ1RYUU)
        - [#ux-social](https://gitlab.slack.com/messages/C01SDUDAGDR)
        - [#ux-community-contributions](https://gitlab.slack.com/messages/C01QPJGU14P)
        - [#figma](https://gitlab.slack.com/messages/CCNDGX396C)
        - There are also UX slack channels for each stage
    - Company
        - [#whats-happening-at-gitlab](https://gitlab.slack.com/messages/C0259241C)
        - [#company-fyi](https://gitlab.slack.com/messages/C010XFJFTHN)
        - [#is-this-known](https://gitlab.slack.com/messages/CETG54GQ0)
        - [#product](https://gitlab.slack.com/messages/C0NFPSFA8)
        - [#eng-week-in-review](https://gitlab.slack.com/messages/CJWA4E9UG)
        - [#handbook](https://gitlab.slack.com/messages/C81PT2ALD)
- [ ] As for the weekly [UX meeting](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit) (every Tuesday), please check your calendar and let your manager know if you don't see an invite. Note that you are not obligated to be part of any of the meetings that you are invited to. However, please note your absence by responding to invites in time. Not attending the meeting is no excuse for not reading the agenda items or going through the notes of the meeting. Oh, and did I mention there are [recordings available](https://drive.google.com/drive/u/0/folders/1VIki1Z3bp8KEoZFq7ABKeKLy9KhHeEgO)? :slight\_smile: 
- [ ] There is a fortnightly [UX Showcase](https://about.gitlab.com/handbook/product/ux/ux-department-workflow/ux-showcase/) to learn what designers at different stage groups are working on with walk throughs of various research findings and design solutions. It is also an opportunity for designers to learn different approaches and processes from one another.
- [ ] Once you are added to the UX Calendar on Google, attend one of our `UX Hangouts` calls. We meet once every two weeks at a rotating time to hang out with other UX members and talk about anything we want.
- [ ] Lastly, every department and stage group host [group conversations](https://about.gitlab.com/handbook/group-conversations/) to keep everyone informed about their efforts. This is a great way to learn about the company and what different teams are up to. Please make sure you have access to the `GitLab Team Meetings` calendar to join any of those. They are called conversations for a reason, meaning you are able to ask questions and join the discussion!

## Your product stage :fox:

**(FILL WITH PRODUCT STAGE NAME)** has been designated your product stage, to be your focus and responsibility. Any issues related to this stage will be labeled (FILL WITH PRODUCT STAGE ~ LABEL). Please make sure you:

- [ ] Understand our [product hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy).
- [ ] Get to know your [stage's teammates](https://about.gitlab.com/handbook/product/categories/#devops-stages).
- [ ] Read its [vision](FILL WITH PRODUCT STAGE VISION PAGE URL).
- [ ] See where it stands in terms of [category maturity](https://about.gitlab.com/direction/maturity/).
- [ ] Explore its [features](FILL WITH PRODUCT STAGE FEATURE URL) and [roadmap](FILL WITH PRODUCT STAGE ROADMAP URL) 
- [ ] Watch product demos and more on [GitLab Learn](https://about.gitlab.com/learn/) 
- [ ] Start collaborating with your assigned UX Buddy. You should both arrange a day to have a regular 1:1 call (at least once a week). They have prepared a [Google Doc](FILL WITH GOOGLE DOC URL) to guide your sessions, feel free to add discussion points to the agenda.


<!--
Below is extra inforomation to help you understand our product better. Feel free to remove or revise.
-->

## Understand GitLab users - how people use our product  :pencil2:
- [ ] Read about user [personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#roles-vs-personas)
- [ ] UX Scorecards: [what are they?](https://about.gitlab.com/handbook/product/ux/ux-scorecards/), [epic link](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=UX%20scorecard)
- [ ] UX Research Shadowing: Prior to performing UX research on your own, you should participate in research shadowing. You can kick off the process when you meet with the [UX Researcher for your group](https://about.gitlab.com/handbook/product/categories/#devops-stages) and learn more about the [UX Research shadowing process](https://about.gitlab.com/handbook/product/ux/ux-research-training/research-shadowing/). 

## Tech knowledge to know :computer:

As a designer at GitLab, you will come into contact with a lot of complex, technical concepts. If you already want to dive deeper into some of these, you can have a look at the following videos.

**Required**

- [ ] What is DevOps: [video](https://youtu.be/_Gpe1Zn-1fE )
- [ ] What is CI(continuous integration)/CD(continuous delivery/deployment): [video](https://www.youtube.com/watch?v=AlrImm1T8Wg)
- [ ] What is YAML file: [video](https://www.youtube.com/watch?v=o9pT9cWzbnI)
- [ ] What is value stream: [direction page](https://about.gitlab.com/solutions/value-stream-management/)

**Optional**

- What is Docker: [video](https://www.youtube.com/watch?v=zJ6WbK9zFpI)
- What is Kubernetes: [video](https://www.youtube.com/watch?v=2vMEQ5zs1kov)
- What is AWS EKS/ECS/Fargate: [video](https://www.youtube.com/watch?v=UT88Ojx-TLk)
- What is the differences between Countinous Delivery VS Countinous Deployment: [video](https://www.youtube.com/watch?v=93raARQl8PE)
- What is Helm: [video](https://www.youtube.com/watch?v=9cwjtN3gkD4)

## GitLab Product: Benefits and Demos :tv:

Optional: If you'd like to learn more about the product outside your stage, the Sales org has created a series of excellent overviews around who uses GitLab and how they benefit from it. Learning about the product from the perspective of benefits will help you understand JTBD too. Feel free to bookmark these and dive deeper after you feel more comfortable.

- Introduction to GitLab: [video](https://www.youtube.com/watch?v=oGedmbCBCp4)
- Learn about competitors: [video](https://www.youtube.com/watch?v=B5xhw5pPWlk)
- Create and Plan stages: [video](https://www.youtube.com/watch?v=C0yrlj9_6Z8)
- Verify and Security stages: [video](https://www.youtube.com/watch?v=YXCu71UwurI)
- Package, Configure and Release stages: [video](https://www.youtube.com/watch?v=fU9ljmeniBY)
- Manage, Monitor and Protect stages: [video](https://www.youtube.com/watch?v=wmdei63TCVM)
- GitLab Workshop 1 (basic demo of GitLab): [video](https://www.youtube.com/watch?v=5mTwIoSS0UU)
- GitLab Workshop 2 (more advanced demo of GitLab): [video](https://www.youtube.com/watch?v=sly0Xe5912g)

<!--

Feel free to add more info and links that may help your new teammate
better understand the product stage, if necessary.

-->

## Access requests :guardsman:

_Note: you can bundle your requests into one [access request issue](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request)_

**UX research**<br>
- [ ] Open a an access request issue to the following UX research systems:
    - [ ] `Dovetail` for UX research insights.
    - [ ] `Respondent.io` for UX research recruitment.
    - [ ] `Qualtrics` for for future research engagements.

**Cloud provider access requests**<br>
If required for your Stage work you can [request access](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) to cloud providers: 
- [ ] GCP projects.
- [ ] AWS.


## Awesome flows :runner:

As part of your onboarding and to be best prepared for future responsibilities, it is recommended to walk through a few flows inside of the product, ask any questions you have (for example in this issue), and also document your steps as you go in a journal. This will give you a chance to reflect back on your onboarding and address/pinpoint issues that may have come up with a new sense of understanding.

Aside from adjusting towards the async workflow of GitLab please try to see if you can complete the following flows:
- [ ] [Creating a project and launching a simple website on GitLab pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html).
- [ ] Suggesting a change and opening a merge request towards any project.
- [ ] See how you receive notifications on issues. You get both emails as well as todos. (It's up to you to decide which one to use — this is a good conversation to have with your UX buddy).
- [ ] Once you have access to the GitLab project in Figma, browse some of the projects to see how we design in Figma. Tip: Click on the ⭑ button to add a project to your favorites (for example, your stage group project).
- [ ] The [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) allows you to install GitLab's development environment on your computer. This will enable you to test certain features that aren't available in production. It is highly recommended that you install the GDK and try checking out a branch to see how it looks running locally. Your UX buddy can help you with this and the #gdk Slack channel is there for you if you need help.
- [ ] If you are not used to maintaining a local development environment like the one mentioned above, you can also use the GDK in [Gitpod](https://gitpod.io). This allows you to make/review changes exclusively in the browser. If you prefer this, read the [instructions on how to use the Gitpod GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitpod.md) and watch this video that walks you through [how to review merge requests both locally as well as in Gitpod](https://www.youtube.com/watch?v=M7b19Dq-1tw&t=12s). The #gitpod-gdk Slack channel is here for you if you need help or have any feedback.
- [ ] Create your first [UX Scorecard](https://about.gitlab.com/handbook/product/ux/ux-scorecards/) documenting the current experience of a common job. Make sure to discuss this with your UX buddy beforehand. They can give you ideas and point to particular product areas that haven't been evaluated yet.
- [ ] Collaborate with your UX buddy and make a change to this [onboarding template](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/.gitlab/issue_templates/UX%20Onboarding.md) to improve the onboarding experience for future GitLab team members.

<!--

Feel free to add specific product stage flows, if necessary.

-->

## Your onboarding experience :open\_hands: 

This issue is [confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#permissions-and-access-to-confidential-issues) and visible only to the GitLab team by default, meaning this is a safe space to ask any questions and to describe any experiences you might have had. It's a tool to help **you** succeed, not others or something else!

To share some new found knowledge, the rest of the UX department would love to hear your experiences. You are in a position to help make things better for everyone (colleagues, new teammates, users, customers, etc.), including you.

1. Start discussions in this issue, one for each specific topic you want to share. These can be about the work itself, the product, the company, the team, or even about your personal side of it all (free time, health, relationships, etc.) To help you with this, here are some _suggestive and optional_ questions:
   - What were the big ideas you came across? The biggest learnings?
   - What were the big surprises?
   - What were the big questions?
   - What went well?
   - What did not go well?
   - What could be better?
1. Some topics will be questions, others statements, and others will be an opportunity to improve. Try to think about what could be improved and, if possible, how. In the end, each possible improvement should either:
   - start a separate issue for a broader discussion,
   - or start a merge request to change something, anything!
1. How can we improve the Product Designer onboarding experience? Please keep notes on what went well, what did not go well, or could be improved. With that information, we'll iteratively improve our onboarding experience.
1. Before closing this issue, please add yourself to the next [UX weekly meeting agenda](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/) and spend 3-5 minutes max in total sharing a summary of your experience with the team.

## Closing thoughts :pray:

Taking in a lot of information can quickly feel overwhelming. This is normal and you are encouraged to ask questions any time. Embrace knowing nothing and you'll take it in as you go. Take your time to work through your onboarding tasks, you are not expected to be at 100% productivity right away! And remember: if you are here, it's because you are the best person we have found in the world (literally) to do this job :smile:

Working remotely comes with great power and great responsibility. [Here are a few tips on being efficient and productive](https://about.gitlab.com/2018/05/17/eliminating-distractions-and-getting-things-done/). If you need inspiration on how to set up a routine that will empower you doing your best work, discover [how other GitLab team members structure their workday](https://gitlab.com/gl-retrospectives/configure/issues/17).

### Remember to practice self-care

Make sure you take breaks if you feel overwhelmed or tired, especially if this is your first remote job. Remember stretch your legs, drink some water, and disconnect when necessary. Try to establish a healthy routine that will empower you to work in a way that enables you to deliver the best results.

---

# UX Buddy tasks
- [ ] Invite new team member to the product stage Slack channel.
- [ ] Add new team member to the product stage daily stand-up bot (your team uses this to post daily updates including what you are working on and if you have any blockers, much like a regular stand-up but much easier than doing one in real life). `*`
- [ ] Add new team member to the product stage [async retrospective](https://gitlab.com/gitlab-org/async-retrospectives). `*`
- [ ] Add new team member to the product stage weekly and monthly meetings. `*`
- [ ] Invite new team member to a 1:1 on their second week and repeating once a week.
- [ ] Create a Google Doc for 1:1 agenda items ([template](https://docs.google.com/document/d/1sg4EtHBGTugxu-u2NSoH9LfE4zXT1ru1-Z3EiIXlohY)), share it with the new team member, attach it to the 1:1 event, and update the link in the [Your product stage](#your-product-stage-fox) section of this issue.
- [ ] Create a [custom Slack emoji](https://gitlab.slack.com/customize/emoji) for the new team member using their profile image and upload it to Slack with the new team member's GitLab handle (for example `:johndoe:`). You can use [makeemoji.com](https://makeemoji.com/) to help you create the emoji with the correct image dimensions. (optional) 

`*` Check with your Product Design Manager or PeopleOps if you need permission to add the new team member.
